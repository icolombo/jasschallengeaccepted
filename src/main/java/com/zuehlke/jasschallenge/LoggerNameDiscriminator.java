package com.zuehlke.jasschallenge;

import ch.qos.logback.classic.spi.ILoggingEvent;

public class LoggerNameDiscriminator extends ch.qos.logback.core.sift.AbstractDiscriminator<ILoggingEvent> {

    public String getDiscriminatingValue(ILoggingEvent iLoggingEvent) {
        return iLoggingEvent.getLoggerName();
    }

    public String getKey() {
        return "loggerName";
    }
}
