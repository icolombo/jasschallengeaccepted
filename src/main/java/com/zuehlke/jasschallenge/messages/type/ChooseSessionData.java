package com.zuehlke.jasschallenge.messages.type;

public class ChooseSessionData {
    private final SessionChoice sessionChoice;
    private final String sessionName;
    private final SessionType sessionType;
    private final boolean asSpectator;
    private final int chosenTeamIndex;

    public ChooseSessionData(SessionChoice sessionChoice, String sessionName, SessionType sessionType, int chosenTeamIndex) {
        this.sessionChoice = sessionChoice;
        this.sessionName = sessionName;
        this.sessionType = sessionType;
        this.asSpectator = false;
        this.chosenTeamIndex = chosenTeamIndex;
    }

    public SessionChoice getSessionChoice() {
        return sessionChoice;
    }

    public String getSessionName() {
        return sessionName;
    }

    public SessionType getSessionType() {
        return sessionType;
    }

    public boolean isAsSpectator() {
        return asSpectator;
    }

    public int getChosenTeamIndex() {
        return chosenTeamIndex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChooseSessionData that = (ChooseSessionData) o;

        if (asSpectator != that.asSpectator) return false;
        if (chosenTeamIndex != that.chosenTeamIndex) return false;
        if (sessionChoice != that.sessionChoice) return false;
        if (sessionName != null ? !sessionName.equals(that.sessionName) : that.sessionName != null) return false;
        return sessionType == that.sessionType;
    }

    @Override
    public int hashCode() {
        int result = sessionChoice != null ? sessionChoice.hashCode() : 0;
        result = 31 * result + (sessionName != null ? sessionName.hashCode() : 0);
        result = 31 * result + (sessionType != null ? sessionType.hashCode() : 0);
        result = 31 * result + (asSpectator ? 1 : 0);
        result = 31 * result + chosenTeamIndex;
        return result;
    }
}
