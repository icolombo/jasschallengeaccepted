package com.zuehlke.jasschallenge

import com.zuehlke.jasschallenge.client.game.strategy.collectors.VictoryCounterJassStrategy
import com.zuehlke.jasschallenge.client.game.strategy.{RandomScalaStrategy, RuleBasedStrategy}
import com.zuehlke.jasschallenge.messages.`type`.SessionType

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}


object BotsBattle extends App {
  val SERVER_URL = args.headOption.getOrElse("ws://localhost:3000")
  val botTypes: Seq[String] = args.lift.apply(1).orElse(Some("random,ruleBased")).map(_.split(",")).get

  val botRunner = new BotRunner(SERVER_URL, SessionType.SINGLE_GAME)

  val clients = botTypes.zipWithIndex.flatMap{
    case ("ruleBased",idx) =>
      botRunner.runBots("ruleBased", 1, idx, () => new RuleBasedStrategy with VictoryCounterJassStrategy)
    case ("random",idx) => botRunner.runBots("random", 1, idx, () => new RandomScalaStrategy)
    case unknownType => throw new RuntimeException(s"unknown type $unknownType")
  }

  Await.result(Future.sequence(clients), 60.minutes)

}
