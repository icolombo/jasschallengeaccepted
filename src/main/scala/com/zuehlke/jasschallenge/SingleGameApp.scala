package com.zuehlke.jasschallenge

import com.zuehlke.jasschallenge.client.RemoteGame
import com.zuehlke.jasschallenge.client.game.Player
import com.zuehlke.jasschallenge.client.game.strategy.collectors.VictoryCounterJassStrategy
import com.zuehlke.jasschallenge.client.game.strategy.{JassStrategy, RandomScalaStrategy, RuleBasedStrategy}
import com.zuehlke.jasschallenge.messages.`type`.SessionType

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future, blocking}


object SingleGameApp extends App {
  val LOCAL_URL = "ws://localhost:3000"

  val player = new Player("icobot", new RuleBasedStrategy)
  new RemoteGame(LOCAL_URL, player, SessionType.SINGLE_GAME, "humans_vs_bots", 1).start()
}
