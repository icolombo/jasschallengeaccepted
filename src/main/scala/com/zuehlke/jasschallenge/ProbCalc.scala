package com.zuehlke.jasschallenge

import com.zuehlke.jasschallenge.client.game.Player
import com.zuehlke.jasschallenge.game.cards.Card
import org.apache.commons.math3.util.CombinatoricsUtils.binomialCoefficient

import scala.util.Random

class ProbCalc[Player,Card](val cardsInPlayByPlayer: Map[Player, Set[Card]], val cardsLeftByPlayer: Map[Player, Int]) {

  lazy val players: Set[Player] = cardsInPlayByPlayer.keySet

  lazy val allCards: Set[Card] =
    cardsInPlayByPlayer.values.reduce(_ union _)

  lazy val cardBuckets: Map[Set[Player], Set[Card]] =
    allCards.groupBy(card => cardsInPlayByPlayer.filter(_._2.contains(card)).keySet)

  lazy val totalPossibilities: Long =
    countPossibilities(cardsLeftByPlayer, cardBuckets.mapValues(_.size))

  lazy val cardBucketsByPlayer: Map[Player, Iterable[Set[Card]]] =
    players.map(player => player -> cardBuckets.filterKeys(_ contains player).values).toMap

  var conditionalProbsForPlayerBuckets: Map[Player,Map[Set[Card],Double]] = Map.empty

  def calculate(player: Player, card: Card): Double = {
    val probOption = conditionalProbsForPlayerBuckets
      .get(player)
      .flatMap(_.filterKeys(_.contains(card)).headOption)
      .map(_._2)

    probOption match {
      case Some(prob) => prob
      case _ =>
        val bucketOption = cardBucketsByPlayer(player).find(_ contains card)
        val prob = bucketOption.map(calculatePossibilitiesForCardsInBucket(player, _))
          .map(_.toDouble / totalPossibilities)
          .getOrElse(0.0)
        val playerBucketsUpdated = conditionalProbsForPlayerBuckets.applyOrElse(player, (_:Player) => Map.empty[Set[Card],Double])
          .updated(bucketOption.getOrElse(Set(card)), prob)
        conditionalProbsForPlayerBuckets = conditionalProbsForPlayerBuckets.updated(player, playerBucketsUpdated)
        prob
    }
  }

  def calculateAny(cards: Set[(Player, Card)]): Double = {
    if (cards.isEmpty)
      0.0
    else {
      val updatedCardsInPlayByPlayer = cards.foldLeft(cardsInPlayByPlayer) { case (cardsInPlay, (player, card)) =>
        cardsInPlay.updated(player, cardsInPlay(player) - card)
      }
      val possibilitiesNegation = new ProbCalc(updatedCardsInPlayByPlayer, cardsLeftByPlayer).totalPossibilities
      (totalPossibilities - possibilitiesNegation).toDouble / totalPossibilities
    }
  }

  def conditional(player: Player, card: Card): ProbCalc[Player,Card] = {
    val updatedCardsInPlayByPlayer = cardsInPlayByPlayer.mapValues(_ - card)
    val updatedCardsLeftByPlayer = cardsLeftByPlayer.updated(player, cardsLeftByPlayer(player) - 1)
    new ProbCalc(updatedCardsInPlayByPlayer, updatedCardsLeftByPlayer)
  }

  def conditionalNot(player: Player, card: Card): ProbCalc[Player,Card] = {
    val updatedCardsInPlayByPlayer = cardsInPlayByPlayer.updated(player, cardsInPlayByPlayer(player) - card)
    new ProbCalc(updatedCardsInPlayByPlayer, cardsLeftByPlayer)
  }

  private def calculatePossibilitiesForCardsInBucket(player: Player, bucket: Set[Card]): Long = {
    val cardsLeft = cardsLeftByPlayer.updated(player, cardsLeftByPlayer(player) - 1)
    val adjustedCardBuckets: Map[Set[Player], Set[Card]] = allCards
      .filterNot(_ == bucket.head)
      .groupBy(card => cardsInPlayByPlayer.filter(_._2.contains(card)).keySet)
    countPossibilities(cardsLeft, adjustedCardBuckets.mapValues(_.size))
  }

  def countPossibilities(cardsLeftByPlayer: Map[Player,Int], buckets: Map[Set[Player], Int]): Long = {
    val player = cardsLeftByPlayer.head._1
    val playerCardBuckets = buckets.filterKeys(_.contains(player)).filterKeys(_ != Set(player))

    val cardsAlreadyFixed = buckets.getOrElse(Set(player), 0)
    val cardsLeft = cardsLeftByPlayer(player) - cardsAlreadyFixed

    val flatBuckets = playerCardBuckets.flatMap(bucket => Seq.fill(bucket._2)(bucket._1)).toSeq
    val partialCounts = for (bucketCombination <- flatBuckets.combinations(cardsLeft)) yield {
      val countsByBucket = bucketCombination.groupBy(identity).mapValues(_.size)
      val numOfPossiblities = countsByBucket.map(bucket => {
        val n = buckets(bucket._1)
        val k = bucket._2
        binomialCoefficient(n, k)
      }).product

      if (cardsLeftByPlayer.tail.nonEmpty) {
        val newBuckets = buckets.map(bucket => bucket._1 -> (bucket._2 - countsByBucket.getOrElse(bucket._1, 0)))
        numOfPossiblities * countPossibilities(cardsLeftByPlayer.tail, removePlayerFromBuckets(player, newBuckets))
      } else {
        numOfPossiblities
      }
    }
    partialCounts.sum
  }

  def removePlayerFromBuckets(player: Player, buckets: Map[Set[Player], Int]): Map[Set[Player], Int] = {
    buckets.toSeq.map {
      case (bucketPlayers, count) => (bucketPlayers - player, count)
    }.groupBy(_._1)
      .mapValues(_.map(_._2).sum)
      .filter(_._2 > 0)
      .filterKeys(_ != Set.empty)
  }


  def simulate(allCards: Set[Card], cardsInPlayByPlayer: Map[Player, Set[Card]], cardsLeftByPlayer: Map[Player,Int], random: Random): Option[Map[Player, Set[Card]]] = {
    var availableCards: Set[Card] = allCards
    var chosenCards: Map[Player, Set[Card]] = Map.empty

    for {
      (player, playerAvailableCards) <- random.shuffle(cardsInPlayByPlayer.toSeq)
    } {
      val playerChosenCards = random.shuffle(playerAvailableCards.intersect(availableCards).toSeq).take(cardsLeftByPlayer(player)).toSet
      availableCards --= playerChosenCards
      chosenCards += player -> playerChosenCards
    }

    if (availableCards.isEmpty) Some(chosenCards)
    else None
  }

}
