package com.zuehlke.jasschallenge
import com.zuehlke.jasschallenge.client.RemoteGame
import com.zuehlke.jasschallenge.client.game.Player
import com.zuehlke.jasschallenge.client.game.strategy.JassStrategy
import com.zuehlke.jasschallenge.messages.`type`.SessionType
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, blocking}

class BotRunner(serverUrl: String, sessionType: SessionType) {
  private val logger = LoggerFactory.getLogger(this.getClass)

  def runBots(teamNamePrefix: String, numberOfTeams: Int, teamIndexOffset: Int, strategyFactory: () => JassStrategy): Seq[Future[Unit]] = {
    createBots(teamNamePrefix, numberOfTeams, teamIndexOffset, strategyFactory = strategyFactory).map(startBot)
  }

  def runBot(playerName: String, strategy: JassStrategy, teamIndex: Int = 0): Future[Unit] = {
    val game = createBot(playerName, strategy, teamIndex)
    startBot(game)
  }

  def createBots(teamNamePrefix: String, numberOfTeams: Int, teamIndexOffset: Int, strategyFactory: () => JassStrategy): Seq[RemoteGame] = {
    for {
      teamId <- 0 until numberOfTeams
      playerId <- 0 to 1
    } yield {
      createBot(s"${teamNamePrefix}_$teamId", strategyFactory(), teamIndexOffset + teamId)
    }
  }

  def createBot(playerName: String, strategy: JassStrategy, teamIndex: Int): RemoteGame = {
    val player = new Player(playerName, strategy)
    new RemoteGame(serverUrl, player, sessionType, teamIndex)
  }

  def startBot(game: RemoteGame): Future[Unit] = {
    Future {
      blocking {
        try {
          game.start()
        } catch {
          case e: Throwable => logger.error(e.getMessage, e)
        }
      }
    }
  }
}
