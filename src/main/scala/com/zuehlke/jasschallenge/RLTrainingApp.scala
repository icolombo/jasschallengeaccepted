package com.zuehlke.jasschallenge

import java.io.File

import com.zuehlke.jasschallenge.client.game.rl4j._
import com.zuehlke.jasschallenge.client.game.strategy.collectors.VictoryCounterJassStrategy
import com.zuehlke.jasschallenge.client.game.strategy.{RandomScalaStrategy, RuleBasedTrumpfChoiceStrategy}
import com.zuehlke.jasschallenge.messages.`type`.SessionType
import org.apache.commons.io.FileUtils
import org.deeplearning4j.rl4j.learning.async.a3c.discrete.{A3CDiscrete, A3CDiscreteDense}
import org.deeplearning4j.rl4j.network.ac.{ActorCriticFactorySeparateStdDense, ActorCriticSeparate}
import org.deeplearning4j.rl4j.util.DataManager
import org.deeplearning4j.util.ModelSerializer.restoreMultiLayerNetwork
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future, blocking}
import scala.util.Try


object RLTrainingApp extends App {

  private val logger = LoggerFactory.getLogger(this.getClass)

  val LOCAL_URL = "ws://localhost:3000"

  val botRunner = new BotRunner(LOCAL_URL, SessionType.SINGLE_GAME)

  val numberOfAgents = 4

  private val mdps = (0 until numberOfAgents).map(_ => new JassMDP)
  private val mdpPool = new PooledJassMDP(mdps)

  var netConfig: ActorCriticFactorySeparateStdDense.Configuration = ActorCriticFactorySeparateStdDense.Configuration.builder
    .l2(0.001)
    .learningRate(0.0005)
    .numHiddenNodes(150)
    .numLayer(2)
    .build

  var a3cConfig: A3CDiscrete.A3CConfiguration = new A3CDiscrete.A3CConfiguration(
    123, //Random seed
    9, //Max step By epoch
    10 * 9 * numberOfAgents, //Max step
    numberOfAgents, //Number of threads
    5, //t_max
    0, //num step noop warmup
    0.01, //reward scaling
    0.99, //gamma
    10.0 //td-error clippingc
  )

  private val valueModelPath = "models/rl/value.model"
  private val policyModelPath = "models/rl/policy.model"
  Try(Seq(valueModelPath, policyModelPath))
    .flatMap(paths => Try(paths.map(path => restoreMultiLayerNetwork(path))))

  val models = Try((restoreMultiLayerNetwork(valueModelPath), restoreMultiLayerNetwork(policyModelPath)))
  val actorCritic = models
    .map(nets => new ActorCriticSeparate(nets._1, nets._2))
    .getOrElse(new ActorCriticFactorySeparateStdDense(netConfig)
      .buildActorCritic(mdpPool.getObservationSpace.getShape, mdpPool.getActionSpace.getSize))

  val a3c: A3CDiscreteDense[EncodableGameState] = new A3CDiscreteDense(mdpPool, actorCritic, a3cConfig, new DataManager(false))

  // training
  val training = Future { blocking {
    a3c.train()
  }}
  var sessionNumber = 0
  while(!training.isCompleted) {
    logger.info(s"starting training session $sessionNumber")
    val myRLClients = mdps.map(mdp => botRunner.runBot("ico", new RLStrategy(mdp)))
    Await.result(Future.sequence(myRLClients), 60.minutes)
    sessionNumber += 1
  }

  FileUtils.touch(new File(valueModelPath))
  FileUtils.touch(new File(policyModelPath))
  a3c.getPolicy.save(valueModelPath, policyModelPath)

  // validation
  logger.info(s"starting validation")
  val myClients = botRunner.runBots("ico", 1, 0, () => new PolicyBasedStrategy(a3c.getPolicy) with VictoryCounterJassStrategy)
  val opponentClients = botRunner.runBots("opponent", 1, 1, () => new RandomScalaStrategy with RuleBasedTrumpfChoiceStrategy)
  Await.result(Future.sequence(myClients ++ opponentClients), 60.minutes)
}
