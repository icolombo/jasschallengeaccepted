package com.zuehlke.jasschallenge.client.game.strategy

import com.zuehlke.jasschallenge.client.game.GameSession
import com.zuehlke.jasschallenge.client.game.strategy.base.ScalaJassStrategy
import com.zuehlke.jasschallenge.game.cards.Card
import com.zuehlke.jasschallenge.game.mode.Mode

import scala.collection.JavaConverters._
import scala.util.Random

class RandomScalaStrategy extends ScalaJassStrategy {

  def chooseTrumpf(availableCards: java.util.Set[Card], session: GameSession, isGschobe: Boolean): Mode = {
    val allPossibleModes = Mode.standardModes
    if (!isGschobe) allPossibleModes.add(Mode.shift)
    allPossibleModes.get(new Random().nextInt(allPossibleModes.size))
  }

  def chooseCard(availableCards: java.util.Set[Card], session: GameSession): Card = {
    val currentGame = session.getCurrentGame
    val round = currentGame.getCurrentRound
    val gameMode = round.getMode
    availableCards.asScala.find(card => gameMode.canPlayCard(card, round.getPlayedCards, round.getRoundColor, availableCards))
      .getOrElse(throw new RuntimeException("There should always be a card to play"))
  }
}