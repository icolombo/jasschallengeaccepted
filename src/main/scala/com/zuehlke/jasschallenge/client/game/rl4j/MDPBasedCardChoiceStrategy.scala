package com.zuehlke.jasschallenge.client.game.rl4j

import com.zuehlke.jasschallenge.client.game.model.GameState
import com.zuehlke.jasschallenge.client.game.strategy.collectors.GameStateCollectorJassStrategy
import com.zuehlke.jasschallenge.game.cards.Card
import org.slf4j.LoggerFactory

trait MDPBasedCardChoiceStrategy extends GameStateCollectorJassStrategy {
  private val logger = LoggerFactory.getLogger(classOf[MDPBasedCardChoiceStrategy].getName + "." + this.hashCode().toString)

  val mdp: JassMDP

  override def gameStarted(gameState: GameState): Unit = {
    mdp.start(gameState)
  }

  override def chooseCard(gameState: GameState): Card = {
    mdp.act(gameState)
  }

  override def roundFinished(gameState: GameState): Unit = {
    val isLastRound = gameState.rounds.size >= 9
    if (!isLastRound) {
      mdp.observe(gameState.currentRoundState.score.get, isDone = false)
    }
  }

  override def gameFinished(gameState: GameState): Unit = {
    mdp.observe(gameState.currentRoundState.score.get, isDone = true)
    mdp.finish(gameState)
  }

}
