package com.zuehlke.jasschallenge.client.game.strategy

import com.zuehlke.jasschallenge.client.game._
import com.zuehlke.jasschallenge.client.game.model.GameState
import org.slf4j.LoggerFactory

class RuleBasedStrategy extends RuleBasedTrumpfChoiceStrategy with RuleBasedCardChoiceStrategy {
  private val logger = LoggerFactory.getLogger(classOf[RuleBasedStrategy].getName + "." + this.hashCode().toString)

  override def onGameStarted(session: GameSession): Unit = {
    logger.debug("---------------------------------------------")
    logger.debug(s"game mode: ${session.getCurrentRound.getMode}")
    super.onGameStarted(session)
  }

  override def onRoundFinished(round: Round): Unit = {
    logger.debug(s"round: ${round.calculateScore()} -> ${round.getWinner.getName}")
    super.onRoundFinished(round)
  }

  override def moveMade(move: Move, gameState: GameState): Unit = {
    if (gameState.session.opponentPlayers contains move.getPlayer) {
      logger.debug(s"opponent played card: ${move.getPlayedCard}")
    }
    if (gameState.session.myPartner == move.getPlayer) {
      logger.debug(s"partner played card: ${move.getPlayedCard}")
    }
    super.moveMade(move, gameState)
  }


  override def gameFinished(gameState: GameState): Unit = {
    val teamScore = session.getCurrentGame.getResult.getTeamScore(myself)
    val otherTeamPlayer = gameState.session.opponentPlayers.head
    val otherTeamScore = session.getCurrentGame.getResult.getTeamScore(otherTeamPlayer)
    logger.debug("game score: " + (teamScore - otherTeamScore))
    super.gameFinished(gameState)
  }

}
