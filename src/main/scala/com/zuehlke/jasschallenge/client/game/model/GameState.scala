package com.zuehlke.jasschallenge.client.game.model

import com.zuehlke.jasschallenge.client.game.{Move, Player, Team}
import com.zuehlke.jasschallenge.game.cards.{Card, Color}
import com.zuehlke.jasschallenge.game.mode.Mode

import scala.collection.JavaConverters._

case class SessionState(myself: Player, teams: Set[Team]) {
  lazy val myTeam: Team = teams
    .find(_.isTeamOfPlayer(myself))
    .getOrElse(throw new RuntimeException("Could not find my team"))

  lazy val myPartner: Player = myTeam.getPlayers.asScala
    .find(_ != myself)
    .getOrElse(throw new RuntimeException("Could not find my partner"))

  lazy val opponentTeam: Team = teams
    .find(!_.isTeamOfPlayer(myself))
    .getOrElse(throw new RuntimeException("Could not find opponent team"))

  lazy val opponentPlayers: Set[Player] = opponentTeam.getPlayers.asScala.toSet

  lazy val otherPlayers: Set[Player] = opponentPlayers + myPartner
}

case class RoundState(
                       moves: Seq[Move],
                       score: Option[Int],
                       session: SessionState
) {

  import session._

  lazy val playedCards: Seq[Card] = moves.map(_.getPlayedCard)

  lazy val partnerCardOption: Option[Card] = playedCardByPlayer(myPartner)

  lazy val myCard: Option[Card] = playedCardByPlayer(myself)

  lazy val opponentCards: Set[Card] = opponentPlayers.flatMap(playedCardByPlayer)

  lazy val roundColorOption: Option[Color] = playedCards
    .headOption
    .map(_.getColor)

  def playedCardByPlayer(player: Player): Option[Card] =
    moves.find(_.getPlayer == player).map(_.getPlayedCard)

}

case class GameState(
                      session: SessionState,
                      mode: Mode,
                      rounds: Seq[RoundState],
                      myInitialHand: Set[Card],
                      isMyTeamsGame: Boolean,
                      isShifted: Boolean
) {


  import session._

  lazy val myCards: Set[Card] = myInitialHand -- rounds.flatMap(_.myCard)

  lazy val currentRoundState: RoundState = rounds.last

  lazy val cardsLeftByPlayer: Map[Player,Int] = otherPlayers.map(player => {
    val numOfCardsPlayed = rounds.count(_.moves.exists(_.getPlayer == player))
    (player, 9 - numOfCardsPlayed)
  }).toMap

  lazy val partnerPlayedCards: Seq[(Color,Card)] = rounds
    .flatMap(round =>
      round.moves
        .find(_.getPlayer == myPartner)
        .map(partnerMove => (round.roundColorOption.get, partnerMove.getPlayedCard)))

  def isDone: Boolean = rounds.size == 9 && rounds.last.moves.size == 4

  def score: Int = rounds.flatMap(_.score).sum
}