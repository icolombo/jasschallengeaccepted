package com.zuehlke.jasschallenge.client.game.rl4j

import com.zuehlke.jasschallenge.game.cards.Card
import org.deeplearning4j.rl4j.space.DiscreteSpace

class JassActionSpace(cards: Seq[Card]) extends DiscreteSpace(cards.size + 1) {
  override def encode(a: Integer): Card = if (a == 0) null else cards(a - 1)
  def decode(card: Card): Int = cards.indexOf(card) + 1

  override def randomAction(): Integer = {
    super.randomAction()
  }

  def actionWeights(validCards: Set[Card]): Array[Double] = {
    val weights = Array.fill(getSize)(0.0)
    validCards.map(decode).foreach {
      weights.update(_, 1.0)
    }
    weights
  }
}
