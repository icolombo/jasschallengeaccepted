package com.zuehlke.jasschallenge.client.game.rl4j

import com.zuehlke.jasschallenge.client.game.GameStateAnalysis
import com.zuehlke.jasschallenge.client.game.model.GameState
import com.zuehlke.jasschallenge.client.game.rl4j.EncodableGameState._
import com.zuehlke.jasschallenge.client.game.strategy.collectors.GameStateCollectorJassStrategy
import com.zuehlke.jasschallenge.game.cards.Card
import org.deeplearning4j.rl4j.learning.Learning
import org.deeplearning4j.rl4j.policy.Policy

trait PolicyBasedCardChoiceStrategy extends GameStateCollectorJassStrategy {
  def policy: Policy[EncodableGameState,Integer]
  val actionSpace: JassActionSpace = new JassActionSpace(Card.values())
  val observationSpace: JassObservationSpace = new JassObservationSpace

  override def chooseCard(gameState: GameState): Card = {
    val input = Learning.getInput[EncodableGameState](gameState, observationSpace)
    val actionWeights = actionSpace.actionWeights(new GameStateAnalysis(gameState).myValidCards)
    actionSpace.encode(policy.nextAction(input, actionWeights))
  }

}
