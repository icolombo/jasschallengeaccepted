package com.zuehlke.jasschallenge.client.game.strategy.collectors

import java.util

import com.zuehlke.jasschallenge.client.game.model.{GameState, RoundState, SessionState}
import com.zuehlke.jasschallenge.client.game.strategy.base.ScalaJassStrategy
import com.zuehlke.jasschallenge.client.game.{GameSession, Move, Player, Round}
import com.zuehlke.jasschallenge.game.cards.Card

import scala.collection.JavaConverters._

trait GameStateCollectorJassStrategy extends ScalaJassStrategy {
  private var _sessionState: SessionState = _
  private var _gameState: GameState = _

  def chooseCard(gameState: GameState): Card

  def gameStarted(gameState: GameState): Unit = {}
  def moveMade(move: Move, gameState: GameState): Unit = {}
  def roundFinished(gameState: GameState): Unit = {}
  def gameFinished(gameState: GameState): Unit = {}

  override def chooseCard(availableCards: util.Set[Card], session: GameSession): Card = {
    chooseCard(_gameState)
  }

  override def onSessionStarted(session: GameSession, myself: Player): Unit = {
    _sessionState = SessionState(myself, session.getTeams.asScala.toSet)

    super.onSessionStarted(session, myself)
  }

  override def onGameStarted(session: GameSession): Unit = {
    val cardsInPlay = Card.values().toSet -- myself.getCards.asScala.toSet

    val roundState = RoundState(Seq.empty, None, _sessionState)

    _gameState = GameState(
      session = _sessionState,
      mode = session.getCurrentGame.getCurrentRound.getMode,
      rounds = Seq(roundState),
      myInitialHand = myself.getCards.asScala.toSet,
      isMyTeamsGame = _sessionState.myTeam.getPlayers.contains(session.getCurrentRound.getPlayingOrder.getCurrentPlayer),
      isShifted = session.getCurrentGame.isShifted
    )

    super.onGameStarted(session)
    gameStarted(_gameState)
  }

  override def onMoveMade(move: Move, session: GameSession) {
    val roundState = _gameState.currentRoundState.copy(moves = session.getCurrentRound.getMoves.asScala)

    _gameState = _gameState.copy(
      rounds = _gameState.rounds.init :+ roundState
    )

    super.onMoveMade(move, session)
    moveMade(move, _gameState)
  }

  override def onRoundFinished(round: Round): Unit = {
    val roundScore = calculateScore(round)
    val roundState = _gameState.currentRoundState.copy(score = Some(roundScore))

    _gameState = _gameState.copy(
      rounds = _gameState.rounds.init :+ roundState
    )

    super.onRoundFinished(round)
    roundFinished(_gameState)

    if (_gameState.rounds.size < 9) {
      _gameState = _gameState.copy(
        rounds = _gameState.rounds :+ RoundState(Seq.empty, None, _sessionState)
      )
    }
  }

  private def calculateScore(round: Round): Int = {
    val score = round.calculateScore()
    val winner = round.getWinner
    if (_sessionState.myTeam.getPlayers contains winner) {
      score
    } else {
      -score
    }
  }

  override def onGameFinished(): Unit = {
    val teamScore = session.getCurrentGame.getResult.getTeamScore(myself)
    val otherTeamScore = session.getCurrentGame.getResult.getTeamScore(_sessionState.opponentPlayers.head)
    val finalScore = teamScore - otherTeamScore

    val roundState = _gameState.currentRoundState.copy(
      score = Some(finalScore - _gameState.rounds.init.flatMap(_.score).sum)
    )
    _gameState.copy(rounds = _gameState.rounds.init :+ roundState)

    super.onGameFinished()
    gameFinished(_gameState)
  }
}

