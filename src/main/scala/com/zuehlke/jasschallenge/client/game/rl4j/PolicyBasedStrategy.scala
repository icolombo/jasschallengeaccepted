package com.zuehlke.jasschallenge.client.game.rl4j

import com.zuehlke.jasschallenge.client.game.strategy.RuleBasedTrumpfChoiceStrategy
import org.deeplearning4j.rl4j.policy.Policy

class PolicyBasedStrategy(val policy: Policy[EncodableGameState,Integer]) extends PolicyBasedCardChoiceStrategy with RuleBasedTrumpfChoiceStrategy {

}
