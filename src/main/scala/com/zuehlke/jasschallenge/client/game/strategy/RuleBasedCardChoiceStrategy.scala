package com.zuehlke.jasschallenge.client.game.strategy

import com.zuehlke.jasschallenge.client.game._
import com.zuehlke.jasschallenge.client.game.model.GameState
import com.zuehlke.jasschallenge.client.game.strategy.collectors.GameStateCollectorJassStrategy
import com.zuehlke.jasschallenge.game.cards.{Card, Color}
import com.zuehlke.jasschallenge.game.mode.Mode
import org.slf4j.LoggerFactory

import scala.collection.JavaConverters._

trait RuleBasedCardChoiceStrategy extends GameStateCollectorJassStrategy {
  private val logger = LoggerFactory.getLogger(classOf[RuleBasedCardChoiceStrategy].getName + "." + this.hashCode().toString)

  override def chooseCard(gameState: GameState): Card = {
    logger.debug("my cards: " + gameState.myCards)
    logger.debug(s"color: ${gameState.currentRoundState.roundColorOption}, moves: ${gameState.currentRoundState.playedCards}")

    val rules = List(
      schmierenRule _,
      attackRule _,
//      übergebenRule _,
//      verwerfenRule _,
      austrumpfenRule _,
      weissnichtRule _
    )

    val analysis = new GameStateAnalysis(gameState)

    val rating = rules.foldLeft(Map[Card,Int]())((acc, rule) => mergeRatings(acc, rule(analysis)))

    val chosenCard = rating.toSeq.maxBy(_._2)._1
    logger.debug("chosen: " + chosenCard)
    chosenCard
  }

  def mergeRatings(rating1: Map[Card, Int], rating2: Map[Card, Int]): Map[Card, Int] = {
    (rating1.toSeq ++ rating2.toSeq)
      .groupBy(_._1)
      .mapValues(_.map(_._2).sum)
  }


  private def schmierenRule(analysis: GameStateAnalysis): Map[Card,Int] = {
    val highScoreCards = analysis.myValidCards.map(card => card -> getScore(analysis.state.mode, card))
      .filter(_._2 >= 8)
      .map(_._1)
    val schmierCards = analysis.partnerRoundWinProbabilityByCard(highScoreCards)
      .filter(_._2 == 1)
      .map { case (card, rating) => card -> (3 * rating / analysis.getSameColorBockProbability(card)) }
      .mapValues(_.ceil.toInt)
    logger.debug("schmieren: " + schmierCards)
    schmierCards
  }



  private def attackRule(analysis: GameStateAnalysis): Map[Card,Int] = {
    val partnerNextRoundWinProb = analysis.partnerNextRoundWinProbability()
    val partnerCardIsBock = analysis.state.currentRoundState.partnerCardOption.exists(partnerCard => analysis.getRoundWinProbability(partnerCard) == 1)
    val attackCards = analysis.myValidCards
      .map(card => card -> analysis.getRoundWinProbability(card))
      .filter(_._2 > 0.8)
      .map{
        case (card,rating) if analysis.state.currentRoundState.moves.isEmpty && card.getColor == analysis.state.mode.getTrumpfColor => card -> (rating * 0.1)
        case (card,rating) if partnerCardIsBock && analysis.state.myCards.size > 1 => card -> (rating * analysis.myNextRoundWinProbability(analysis.state.myCards - card) / partnerNextRoundWinProb)
        case (card,rating) if card.getColor == analysis.state.mode.getTrumpfColor => card -> (rating * getScore(analysis.state.mode, analysis.state.currentRoundState.playedCards:_*) / 20)
        case (card,rating) => card -> rating
      }
      .toMap
      .mapValues(_.*(10).ceil.toInt)
      logger.debug("attack: " + attackCards)
      attackCards
  }

  private def übergebenRule(analysis: GameStateAnalysis): Map[Card,Int] = {
    if (analysis.state.currentRoundState.moves.isEmpty) {
      val validCards = analysis.myValidCards
      val myRoundWinProbability = validCards.map(analysis.getRoundWinProbability).reduceOption(_ max _).getOrElse(0.0)
      val partnerRoundWinProbabilities = analysis.partnerRoundWinProbabilityByCard(validCards)
      logger.debug(myRoundWinProbability.toString)
      logger.debug(partnerRoundWinProbabilities.toString)
      val übergebenCards = partnerRoundWinProbabilities
        .filter(_._2 > myRoundWinProbability)
        .map { case (card, prob) => card -> (prob * (1 - analysis.getSameColorBockRating(card))) }
        .mapValues(_.*(2).ceil.toInt)
      logger.debug("übergeben: " + übergebenCards)
      übergebenCards
    }
    else Map()
  }


  private def verwerfenRule(analysis: GameStateAnalysis): Map[Card,Int] = {
    val validCards = analysis.myValidCards
    if (analysis.state.currentRoundState.roundColorOption.exists(roundColor => !validCards.exists(_.getColor == roundColor))) {
      val cardBockProbabilities = validCards.map(card => card -> analysis.getSameColorBockProbability(card))
      val colorRatings = cardBockProbabilities
        .groupBy(_._1.getColor)
        .mapValues(_.map(_._2))
        .mapValues(cardProbs => cardProbs.sum.ceil.toInt + cardProbs.size)
      val lowestRatedCardByColor = cardBockProbabilities
        .map{case (card,prob) => card -> prob / getScore(analysis.state.mode, card).max(1)}
        .groupBy(_._1.getColor)
        .mapValues(_.minBy(_._2)._1)
      val verwerfenCards = lowestRatedCardByColor.values.map(card => card -> colorRatings(card.getColor)).toMap
      logger.debug("verwerfen: " + verwerfenCards)
      verwerfenCards
    }
    else
      Map()
  }

  private def austrumpfenRule(analysis: GameStateAnalysis): Map[Card,Int] = {
    val validCards = analysis.myValidCards
    if(analysis.state.isMyTeamsGame
        && analysis.state.currentRoundState.moves.isEmpty
        && analysis.state.mode.getTrumpfColor != null) {
      val austrumpfenCards = if (analysis.state.session.opponentPlayers.exists(hasColor(analysis, _, analysis.state.mode.getTrumpfColor))) {
        val bockTrumpfCards = validCards.filter(_.getColor == analysis.state.mode.getTrumpfColor).filter(analysis.isBock)
        val bestBockTrumpf = bockTrumpfCards.toSeq.sortWith(!_.isHigherTrumpfThan(_)).lastOption
        bestBockTrumpf.map(_ -> 11).toMap
      } else if (analysis.state.session.opponentPlayers.forall(hasColor(analysis, _, analysis.state.mode.getTrumpfColor))) {
        val trumpfCards = validCards.filter(_.getColor == analysis.state.mode.getTrumpfColor)
        val bestTrumpf = trumpfCards.toSeq.sortWith(!_.isHigherTrumpfThan(_)).lastOption
        bestTrumpf.map(_ -> 10).toMap
      } else Map.empty[Card,Int]
      logger.debug("austrumpfen: " + austrumpfenCards)
      austrumpfenCards
    }
    else Map.empty[Card,Int]
  }

  private def hasColor(analysis: GameStateAnalysis, otherPlayer: Player, color: Color): Boolean = {
    analysis.cardsInPlayByPlayer(otherPlayer).exists(_.getColor == color)
  }

  private def weissnichtRule(analysis: GameStateAnalysis): Map[Card,Int] = {
    val weissnichtCards = analysis.myValidCards
      .map{
        case card if card.getColor == analysis.state.mode.getTrumpfColor => card -> 0.0
        case card =>
          card -> (1 - analysis.getSameColorBockRating(card)) / getScore(analysis.state.mode, card).max(1)
      }
      .toMap
      .mapValues(_.*(3).ceil.toInt)
    logger.debug("weissnicht: " + weissnichtCards)
    weissnichtCards
  }

  private def getScore(mode: Mode, cards: Card*) =
    mode.calculateScore(cards.toSet.asJava) / mode.getFactor
}
