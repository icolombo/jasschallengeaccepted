package com.zuehlke.jasschallenge.client.game.strategy.collectors

import com.zuehlke.jasschallenge.client.game.GameSession
import com.zuehlke.jasschallenge.client.game.strategy.base.ScalaJassStrategy
import org.slf4j.LoggerFactory

import scala.collection.JavaConverters._

trait VictoryCounterJassStrategy extends ScalaJassStrategy {
  private val logger = LoggerFactory.getLogger(classOf[VictoryCounterJassStrategy])

  private var lastScoreDelta: Int = 0
  private var numGames = 0
  private var numWins = 0

  override def onGameStarted(session: GameSession) {
    lastScoreDelta = 0

    super.onGameStarted(session)
  }

  override def onGameFinished() {
    val currentScoreDelta = currentSessionScoreDelta()

//    if (myself.getId.hashCode < myPartner.getId.hashCode) {
      numGames += 1
      val gameScoreDelta = currentScoreDelta - lastScoreDelta
      if (gameScoreDelta > 0) {
        numWins += 1
      }
//    }

    lastScoreDelta = currentScoreDelta

    super.onGameFinished()
  }

  override def onSessionFinished(): Unit = {
    if (numGames != 0) {
      logger.info(s"Wins/Games: $numWins/$numGames (${100 * numWins / numGames}%)")
    }
    super.onSessionFinished()
  }

  private def currentSessionScoreDelta(): Int = {
    val teamScore = session.getCurrentGame.getResult.getTeamScore(myself)
    val otherTeamPlayer = session.getTeams.asScala.find(!_.isTeamOfPlayer(myself)).get.getPlayers.asScala.head
    val otherTeamScore = session.getCurrentGame.getResult.getTeamScore(otherTeamPlayer)
    teamScore - otherTeamScore
  }

}

