package com.zuehlke.jasschallenge.client.game.strategy.collectors

import com.zuehlke.jasschallenge.client.game.strategy.base.ScalaJassStrategy
import com.zuehlke.jasschallenge.client.game.{GameSession, Move, Player, Round}
import com.zuehlke.jasschallenge.game.cards.Card
import org.slf4j.LoggerFactory

case class Situation(hand: Set[Card], table: Set[Card], played: Set[Card])

trait DataCollectorJassStrategy extends ScalaJassStrategy {
  final private val logger = LoggerFactory.getLogger(classOf[DataCollectorJassStrategy])

  private var gamePlayedCards: Set[Card] = Set.empty
  private var gameSituations: Set[Situation] = Set.empty

  override def onSessionStarted(session: GameSession, myself: Player) {
    super.onSessionStarted(session, myself)
    logger.info("onSessionStarted" + myself)
  }

  override def onGameStarted(session: GameSession) {
    gameSituations = Set.empty
    gamePlayedCards = Set.empty
    logger.info("onGameStarted" + myself)
    super.onGameStarted(session)
  }

  override def onMoveMade(move: Move, session: GameSession) {
    gamePlayedCards += move.getPlayedCard
    logger.info("onMoveMade" + myself)
    super.onMoveMade(move, session)
  }

  override def onRoundFinished(round: Round): Unit = {
    logger.info("onRoundFinished" + myself)
    super.onRoundFinished(round)
  }

  override def onGameFinished() {
    logger.info("onGameFinished" + myself)
    super.onGameFinished()
  }

  override def onSessionFinished() {
    logger.info("onSessionFinished" + myself)
    super.onSessionFinished()
  }

}

