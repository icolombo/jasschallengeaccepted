package com.zuehlke.jasschallenge.client.game.strategy

import java.util

import com.zuehlke.jasschallenge.client.game._
import com.zuehlke.jasschallenge.client.game.strategy.base.ScalaJassStrategy
import com.zuehlke.jasschallenge.game.Trumpf
import com.zuehlke.jasschallenge.game.cards.{Card, Color}
import com.zuehlke.jasschallenge.game.mode.Mode
import org.slf4j.LoggerFactory

import scala.collection.JavaConverters._

trait RuleBasedTrumpfChoiceStrategy extends ScalaJassStrategy {
  private val logger = LoggerFactory.getLogger(classOf[RuleBasedTrumpfChoiceStrategy].getName + "." + this.hashCode().toString)

  override def chooseTrumpf(availableCards: util.Set[Card], session: GameSession, isGschobe: Boolean): Mode = {
    val modesAndScores = Mode.standardModes().asScala.map(mode => (mode, calculateScoreForTrumpfChoice(availableCards.asScala.toSet, mode)))
    val (mode,score) = modesAndScores.maxBy(_._2)
    logger.debug("trumpf choice: " + modesAndScores)
    if (score >= 10 || isGschobe) mode
    else Mode.shift()
  }


  private def calculateScoreForTrumpfChoice(cards: Set[Card], mode: Mode) = {
    // TODO still not good, choose according to rank maybe?
    val colorRanking = Color.values().toSet[Color].map(color => color -> {
      val sortedColors = cardsOfColor(color).toSeq.sortWith(!GameStateAnalysis.isHigherCardSameColor(_, _, mode))
      val ranking = sortedColors.zipWithIndex.toMap
      ranking.filterKeys(cards.contains).values.sum
    }).toMap
    logger.debug("mode rank: " + mode + "  " + colorRanking)

    val modeRating = if (mode.getTrumpfName == Trumpf.TRUMPF) {
      colorRanking(mode.getTrumpfColor)
    } else {
      colorRanking.values.sum / 4
    }
    val bockBonus = cards.count(GameStateAnalysis.isBock(_, Card.values().toSet -- cards, mode))
    logger.debug("bockbonus: " + bockBonus)
    logger.debug("mode rating: " + (modeRating + bockBonus))

    modeRating + bockBonus

//    val filteredCards = cards.filterNot(Set(CardValue.EIGHT, CardValue.TEN) contains _.getValue)
//    mode.calculateScore(filteredCards) / mode.getFactor
  }

  def cardsOfColor(color: Color): Set[Card] = {
    Card.values().toSet.filter(_.getColor == color)
  }

}
