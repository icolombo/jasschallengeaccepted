package com.zuehlke.jasschallenge.client.game.strategy.collectors

import com.zuehlke.jasschallenge.client.game.Round
import com.zuehlke.jasschallenge.client.game.strategy.base.ScalaJassStrategy
import org.slf4j.LoggerFactory
import scala.collection.JavaConverters._

trait RoundDataCollectorJassStrategy extends ScalaJassStrategy {
  final private val stichLogger = LoggerFactory.getLogger("LEARN.STICH")

  override def onRoundFinished(round: Round): Unit = {
    if (round.getWinner == myself) {
      val winnerCard = round.getMoves.asScala.find(_.getPlayer == myself).get.getPlayedCard
      val cards = round.getMoves.asScala.map(_.getPlayedCard)
      stichLogger.info(s"${round.getMode},${cards.mkString(",")},$winnerCard")
    }
    super.onRoundFinished(round)
  }

}

