package com.zuehlke.jasschallenge.client.game.rl4j

import java.util.concurrent.atomic.AtomicReference

import com.zuehlke.jasschallenge.client.game.model.GameState
import org.deeplearning4j.gym.StepReply
import org.deeplearning4j.rl4j.mdp.MDP
import org.deeplearning4j.rl4j.space.{DiscreteSpace, Encodable}
import org.slf4j.LoggerFactory

import scala.concurrent.duration._
import scala.concurrent.{Await, Promise}

trait ControllableMDP[O <: Encodable,A,AS <: DiscreteSpace] extends MDP[O, Integer, AS] {
  private val logger = LoggerFactory.getLogger(classOf[ControllableMDP[_,_,_]].getName + "." + this.hashCode().toString)

  private val currentObservation: AtomicReference[Promise[O]] = new AtomicReference(Promise[O])
  private val nextAction: AtomicReference[Promise[A]] = new AtomicReference(Promise[A])
  private val currentReward: AtomicReference[Promise[Double]] = new AtomicReference(Promise[Double])
  private val isDoneAtomic: AtomicReference[Boolean] = new AtomicReference(false)
  private val startGame: AtomicReference[Promise[Unit]] = new AtomicReference(Promise[Unit]())

  def start(gameState: GameState): Unit = {
    logger.debug("start")
    swapBlocking(startGame)
  }

  def act(observation: O): A = {
    logger.debug("act")
    currentObservation.get().success(observation)
    swapBlocking(nextAction)
  }

  def observe(reward: Double, isDone: Boolean): Unit = {
    logger.debug("observe")
    currentReward.get().success(reward)
    isDoneAtomic.set(isDone)
  }

  def finish(observation: O): Unit = {
    logger.debug("finish")
    currentObservation.get().success(observation)
  }

  override def isDone: Boolean =
    isDoneAtomic.get()

  override def reset(): O = {
    logger.debug("reset")
    isDoneAtomic.set(false)
    startGame.get().success(())
    swapBlocking(currentObservation)
  }

  override def step(action: Integer): StepReply[O] = {
    logger.debug("step")
    nextAction.get().success(getActionSpace.encode(action).asInstanceOf[A])
    val newObservation = swapBlocking(currentObservation)
    val lastReward = swapBlocking(currentReward)
    new StepReply(newObservation, lastReward, isDone, null)
  }

  private def swapBlocking[T](promise: AtomicReference[Promise[T]]): T = {
    val res = Await.result(promise.get().future, 5.seconds)
    promise.set(Promise())
    res
  }
}
