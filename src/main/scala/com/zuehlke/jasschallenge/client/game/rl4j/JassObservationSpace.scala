package com.zuehlke.jasschallenge.client.game.rl4j

import org.deeplearning4j.rl4j.space.ObservationSpace
import org.nd4j.linalg.api.ndarray.INDArray
import org.nd4j.linalg.factory.Nd4j

class JassObservationSpace extends ObservationSpace[EncodableGameState] {
  override def getName: String = "Jass"

  override def getShape: Array[Int] = EncodableGameState.shape

  override def getLow: INDArray = Nd4j.create(getShape:_*)

  override def getHigh: INDArray = Nd4j.create(getShape:_*)
}
