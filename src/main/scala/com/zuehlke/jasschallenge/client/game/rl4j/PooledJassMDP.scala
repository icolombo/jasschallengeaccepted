package com.zuehlke.jasschallenge.client.game.rl4j

class PooledJassMDP(pool: Seq[JassMDP]) extends JassMDP {
  private var currentInstance: Int = -1

  override def newInstance(): JassMDP = {
    currentInstance += 1
    pool(currentInstance)
  }

}
