package com.zuehlke.jasschallenge.client.game.strategy.base

import com.zuehlke.jasschallenge.client.game.strategy.JassStrategy
import com.zuehlke.jasschallenge.client.game._
import com.zuehlke.jasschallenge.game.cards.{Card, Color}
import com.zuehlke.jasschallenge.game.mode.Mode

import scala.collection.JavaConversions._

trait ScalaJassStrategy extends JassStrategy {
  private var _session: GameSession = _
  private var _myself: Player = _

  def session = _session
  def myself = _myself


  override def onSessionStarted(session: GameSession, myself: Player): Unit = {
    this._session = session
    this._myself = myself
  }

  override def onGameStarted(session: GameSession): Unit = ()

  override def onMoveMade(move: Move, session: GameSession): Unit = ()

  override def onRoundFinished(round: Round): Unit = ()

  override def onGameFinished(): Unit = ()

  override def onSessionFinished(): Unit = ()
}
