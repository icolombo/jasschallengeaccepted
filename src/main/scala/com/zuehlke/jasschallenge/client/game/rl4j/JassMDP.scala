package com.zuehlke.jasschallenge.client.game.rl4j

import com.zuehlke.jasschallenge.client.game.GameStateAnalysis
import com.zuehlke.jasschallenge.game.cards.Card
import org.deeplearning4j.rl4j.space.DiscreteSpace

class JassMDP extends ControllableMDP[EncodableGameState, Card, DiscreteSpace] {
  override val getActionSpace: JassActionSpace = new JassActionSpace(Card.values())
  override def close(): Unit = {}
  override def getObservationSpace: JassObservationSpace = new JassObservationSpace

  override def newInstance(): JassMDP = ???

  override def actionWeights(observation: EncodableGameState): Array[Double] = {
    getActionSpace.actionWeights(new GameStateAnalysis(observation.gameState).myValidCards)
  }



}
