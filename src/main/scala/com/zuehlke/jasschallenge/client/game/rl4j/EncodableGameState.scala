package com.zuehlke.jasschallenge.client.game.rl4j

import com.zuehlke.jasschallenge.ProbCalc
import com.zuehlke.jasschallenge.client.game.{GameStateAnalysis, Player}
import com.zuehlke.jasschallenge.client.game.model.GameState
import com.zuehlke.jasschallenge.game.cards.{Card, Color}
import com.zuehlke.jasschallenge.game.mode.Mode
import org.deeplearning4j.rl4j.space.Encodable

import scala.language.implicitConversions
import scala.collection.JavaConverters._

object EncodableGameState {
  val shape: Array[Int] = Array(6 + 6*36 + 4)

  implicit def fromGameState(gameState: GameState): EncodableGameState = EncodableGameState(gameState)
}

case class EncodableGameState(gameState: GameState) extends Encodable {
  lazy val toArray: Array[Double] = {
    val probs = new GameStateAnalysis(gameState).cardProbs

    val encodedMode = nHot(Mode.standardModes.asScala.map(_.toString), gameState.mode.toString)
    val encodedHandCards = nHot(gameState.myCards)
    val encodedPartnerCards = cardProbs(probs, gameState.session.myPartner)
    val encodedOpponentCards = gameState.session.opponentPlayers.toArray.flatMap(cardProbs(probs, _))
    val encodedPartnerTableCard = nHot(gameState.currentRoundState.partnerCardOption.toSet)
    val encodedOpponentTableCards = nHot(gameState.currentRoundState.opponentCards)
    val encodedRoundColor = nHot(Color.values(), gameState.currentRoundState.roundColorOption.toSeq:_*)

    val encoded = encodedMode ++
      encodedHandCards ++
      encodedPartnerCards ++
      encodedOpponentCards ++
      encodedPartnerTableCard ++
      encodedOpponentTableCards ++
      encodedRoundColor

    encoded
  }

  private def cardProbs(probs: ProbCalc[Player, Card], player: Player): Array[Double] = {
    Card.values().map(probs.calculate(player, _))
  }

  def nHot(hotCards: Set[Card]): Array[Double] =
    nHot(Card.values(), hotCards.toSeq:_*)

  def nHot[T](all: Seq[T], hot: T*): Array[Double] = {
    all.map(some => if (hot contains some) 1.0 else 0.0).toArray
  }
}
