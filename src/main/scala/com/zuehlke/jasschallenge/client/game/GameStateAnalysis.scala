package com.zuehlke.jasschallenge.client.game

import com.zuehlke.jasschallenge.ProbCalc
import com.zuehlke.jasschallenge.client.game.model.{GameState, RoundState}
import com.zuehlke.jasschallenge.game.Trumpf
import com.zuehlke.jasschallenge.game.cards.{Card, CardValue, Color}
import com.zuehlke.jasschallenge.game.mode.Mode

import scala.collection.JavaConverters._

object GameStateAnalysis {

  def isHigherCardSameColor(card: Card, otherCard: Card, mode: Mode): Boolean = {
    assert(card.getColor == otherCard.getColor)
    isHigherCard(card, otherCard, mode, card.getColor)
  }

  def isHigherCard(card: Card, otherCard: Card, mode: Mode, roundColor: Color): Boolean = {
    card == determineWinningCard(mode, Seq(getSmallestRankCard(mode, roundColor), card, otherCard))
  }

  def isBock(card: Card, otherCards: Set[Card], mode: Mode): Boolean = {
    determineWinningCard(mode, card +: otherCards.toSeq) == card
  }

  def determineWinningCard(mode: Mode, cards: Seq[Card]): Card =
    mode.determineWinningCard(cards.asJava)

  def getSmallestRankCard(mode: Mode, color: Color): Card = {
    Card.values()
      .filter(_.getColor == color)
      .minBy(_.getValue.getRank * (if (mode.getTrumpfName == Trumpf.UNDEUFE) -1 else 1))
  }
}

class GameStateAnalysis(val state: GameState) {
  import GameStateAnalysis._
  import state.session._
  import state._

  val currentRound: RoundState = rounds.last
  val cardProbs = new ProbCalc(cardsInPlayByPlayer, cardsLeftByPlayer)

  lazy val myValidCards: Set[Card] = filterValidCards(myCards, currentRound.playedCards)

  lazy val cardsInPlayByPlayer: Map[Player,Set[Card]] = {
    val initial: Map[Player,Set[Card]] = otherPlayers.map(_ -> (Card.values().toSet -- myInitialHand)).toMap
    val result = state.rounds.foldLeft(initial)((acc, round) => acc.map {
      case (player, cardsInPlayBefore) => player -> calculateCardsInPlayAfterRound(player, cardsInPlayBefore, round)
    })
    result
  }

  lazy val cardsInPlayByOthers: Set[Card] = cardsInPlayByPlayer.values.flatten.toSet

  def calculateCardsInPlayAfterRound(player: Player, cardsInPlayBeforeRound: Set[Card], roundState: RoundState): Set[Card] = {
    roundState.roundColorOption.flatMap(roundColor =>
      roundState.playedCardByPlayer(player).map(playedCard =>
        if (roundColor != playedCard.getColor && (mode.getTrumpfName != Trumpf.TRUMPF || playedCard.getColor != mode.getTrumpfColor)) {
          cardsInPlayBeforeRound.filter(card => card.getColor != roundState.roundColorOption.get || isTrumpfBuur(card))
        } else {
          cardsInPlayBeforeRound
        }
      )
    ).getOrElse(cardsInPlayBeforeRound) -- roundState.playedCards
  }
  private def isTrumpfBuur(card: Card): Boolean = card.getColor == mode.getTrumpfColor && card.getValue == CardValue.JACK

  def partnerRoundWinProbabilityByCard(myCards: Set[Card]): Map[Card,Double] = {
    currentRound.partnerCardOption
      .map(partnerCard => {
        val partnerCardWinProb = getRoundWinProbability(partnerCard)
        myCards.map(myCard => myCard -> (if (isHigherCard(myCard, partnerCard, mode, currentRound.roundColorOption.get)) 0.0 else partnerCardWinProb))
      })
      .getOrElse {
        myCards.map(myCard => myCard -> {
          val validPartnerCards = filterValidCards(cardsInPlayByPlayer(myPartner), currentRound.playedCards :+ myCard)
          roundWinProb(myPartner, validPartnerCards.toSeq, currentRound.playedCards :+ myCard, cardProbs)
        })
      }.toMap
  }

  def partnerRoundWinProbability(): Double = {
    currentRound.partnerCardOption
      .map(partnerCard => getRoundWinProbability(partnerCard))
      .getOrElse {
        val validPartnerCards = filterValidCards(cardsInPlayByPlayer(myPartner), currentRound.playedCards)
        roundWinProb(myPartner, validPartnerCards.toSeq, currentRound.playedCards, cardProbs)
      }
  }

  def getRoundWinProbability(card: Card): Double = {
    getRoundWinProbability(card, currentRound.playedCards)
  }

  def getRoundWinProbability(card: Card, playedCards: Seq[Card]): Double = {
    val roundColor = (playedCards :+ card).head.getColor
    if (determineWinningCard(mode, playedCards :+ card) != card) return 0 // does this work for cards already played?

    val playersAlreadyPlayed = currentRound.moves.map(_.getPlayer)
    val opponentPlayersStillToPlay = opponentPlayers -- playersAlreadyPlayed

    getRoundBockProbability(card, roundColor, opponentPlayersStillToPlay)
  }

  def isBockInRound(card: Card): Boolean = {
    val playersAlreadyPlayed = currentRound.moves.map(_.getPlayer)
    val playersStillToPlay = otherPlayers -- playersAlreadyPlayed
    val potentialBockCards = playersStillToPlay.flatMap(cardsInPlayByPlayer(_)) ++ currentRound.playedCards

    val roundColor = currentRound.roundColorOption.getOrElse(card.getColor)
    isWinningCard(card, potentialBockCards, roundColor, mode)
  }


  def partnerCardsVerworfen: Seq[Card] = {
    partnerPlayedCards
      .filter(colorAndCard => colorAndCard._1 != colorAndCard._2.getColor)
      .filter(_._2.getColor != mode.getTrumpfColor)
      .map(_._2)
  }



  def myNextRoundWinProbability(cards: Set[Card]): Double = {
    cards
      .map(getRoundWinProbability(_, Seq.empty))
      .max
  }

  def partnerNextRoundWinProbability(): Double = {
    roundWinProb(myPartner, cardsInPlayByPlayer(myPartner).toSeq, Seq.empty, cardProbs)
  }

  def roundWinProb(player: Player, cards: Seq[Card], playedCards: Seq[Card], cardProbs: ProbCalc[Player,Card]): Double = cards match {
    case headCard :: cardsTail =>
      val cardProb = cardProbs.calculate(player, headCard)
      cardProb * roundWinProb(player, cardsTail, playedCards, cardProbs.conditional(player, headCard))
      + (1.0 - cardProb) * roundWinProb(player, cardsTail, playedCards, cardProbs.conditionalNot(player, headCard))
    case _ => cardProbs.cardsInPlayByPlayer(player).map(getRoundWinProbability(_, playedCards)).reduceOption(_ max _).getOrElse(0.0)
  }


  def getSameColorBockProbability(card: Card): Double = {
    val opponentCards = opponentPlayers
      .flatMap(player => cardsInPlayByPlayer(player).map(player -> _))
      .filter{case (_,c) => c.getColor == card.getColor}
      .filter{case (_,c) => isHigherCardSameColor(c, card, mode)}
    1.0 - cardProbs.calculateAny(opponentCards)
  }

  def getSameColorBockRating(card: Card): Double = {
    val higherCardProbs = opponentPlayers
      .flatMap(player => cardsInPlayByPlayer(player).map(player -> _))
      .filter{case (_,c) => c.getColor == card.getColor}
      .filter{case (_,c) => isHigherCardSameColor(c, card, mode)}
      .groupBy(_._2)
      .mapValues(cardProbs.calculateAny)

    1.0 - (higherCardProbs.values.sum / 8)
  }

  private def getRoundBockProbability(card: Card, roundColor: Color, players: Set[Player]) = {
    val higherCards = players.flatMap(player => {
      val availableCards = cardsInPlayByPlayer(player)
      val validCards = filterValidCards(availableCards, currentRound.playedCards :+ card)
      val higher = validCards.filter(isHigherCard(_, card, mode, roundColor))
      higher.map(card => player -> card)
    })
    1.0 - cardProbs.calculateAny(higherCards)
  }

  def isWinningCard(card: Card, otherCards: Set[Card], roundColor: Color, mode: Mode): Boolean = {
    card == determineWinningCard(mode, getSmallestRankCard(mode, roundColor) +: card +: otherCards.toSeq)
  }

  def isBock(card: Card): Boolean = {
    GameStateAnalysis.isBock(card, cardsInPlayByOthers, mode)
  }


  def filterValidCards(availableCards: Set[Card], playedCards: Seq[Card]): Set[Card] = {
    availableCards.filter(card => mode.canPlayCard(card, playedCards.toSet.asJava, playedCards.headOption.map(_.getColor).orNull, availableCards.asJava))
  }
}
