package com.zuehlke.jasschallenge

import org.junit.runner.RunWith
import org.scalatest.Matchers
import org.scalatest.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class ProbCalcTest extends org.scalatest.FunSuite with Matchers {

  type Player = Int
  type Card = Int

  //  val cardsInPlayByPlayer: Map[Player, Set[Card]] = Map(
  //    0 -> Set(0, 1, 2, 3),
  //    1 -> Set(4, 5, 6, 7),
  //    2 -> Set(0, 1, 2, 3, 4, 5, 6, 7)
  //  )

  //    val cardsInPlayByPlayer: Map[Player, Set[Card]] = Map(
  //      0 -> Set(0, 1, 2, 3, 4, 5, 6, 7),
  //      1 -> Set(0, 1, 2, 3, 4, 5, 6, 7),
  //      2 -> Set(0, 1, 2, 3, 4, 5, 6, 7)
  //    )




  test("calculate") {

    val cardsInPlayByPlayer: Map[Player, Set[Card]] = Map(
      0 -> Set(0, 1, 2, 3,    5, 6   ),
      1 -> Set(0, 1, 2, 3, 4         ),
      2 -> Set(0, 1, 2, 3, 4, 5,    7)
    )
    val cardsLeftByPlayer: Map[Player, Int] = Map(
      0 -> 3,
      1 -> 3,
      2 -> 2
    )

    val probCalc = new ProbCalc[Player,Card](cardsInPlayByPlayer, cardsLeftByPlayer)

    assert(probCalc.calculate(0, 1) === (0.3181 +- 0.0001))
    assert(probCalc.calculate(1, 4) === (0.8181 +- 0.0001))
    assert(probCalc.calculate(0, 6) === 1.0)
    assert(probCalc.calculate(0, 7) === 0.0)
  }

  test("calculateAny") {

    val cardsInPlayByPlayer: Map[Player, Set[Card]] = Map(
      0 -> Set(0, 1, 2, 3,    5, 6   ),
      1 -> Set(0, 1, 2, 3, 4         ),
      2 -> Set(0, 1, 2, 3, 4, 5,    7)
    )
    val cardsLeftByPlayer: Map[Player, Int] = Map(
      0 -> 3,
      1 -> 3,
      2 -> 2
    )

    val probCalc = new ProbCalc[Player,Card](cardsInPlayByPlayer, cardsLeftByPlayer)

    assert(probCalc.calculateAny(Set(0 -> 1, 0 -> 6)) === 1.0)
    assert(probCalc.calculateAny(Set(0 -> 0, 0 -> 1, 0 -> 2, 0 -> 3)) === 1.0)
    assert(probCalc.calculateAny(Set(0 -> 0, 0 -> 1)) === (0.5909 +- 0.0001))
  }

  test("simulate") {

    val cardsInPlayByPlayer: Map[Player, Set[Card]] = Map(
      0 -> Set(0, 1, 2, 3,    5, 6   ),
      1 -> Set(0, 1, 2, 3, 4         ),
      2 -> Set(0, 1, 2, 3, 4, 5,    7)
    )
    val cardsLeftByPlayer: Map[Player, Int] = Map(
      0 -> 3,
      1 -> 3,
      2 -> 2
    )
    val probCalc = new ProbCalc[Player,Card](cardsInPlayByPlayer, cardsLeftByPlayer)

    val players: Set[Player] = cardsInPlayByPlayer.keySet
    lazy val allCards: Set[Card] = cardsInPlayByPlayer.values.reduce(_ union _)
    val rnd = new Random(System.nanoTime)

    time {
      val simulations = Stream.continually(probCalc.simulate(allCards, cardsInPlayByPlayer, cardsLeftByPlayer, rnd))
        .filter(_.isDefined)
        .map(_.get)
      println(simulations.take(5000).count(_.apply(0).contains(0)) / 5000.0)
    }
  }

  def time[R](block: => R): R = {
    val t0 = System.nanoTime()
    val result = block    // call-by-name
    val t1 = System.nanoTime()
    println("Elapsed time: " + (t1 - t0) + "ns")
    result
  }
}
